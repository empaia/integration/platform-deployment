# System Setup

The initial setup has to be performed only once.

## Install Required Software

The EMPAIA Platform Deployment can be used on Linux and Windows. Please follow the specific guidelines depending on the operating system. All tutorial sections use `bash` commands.

### Ubuntu 24.04

The EMPAIA Platform Deployment supports [Docker Engine](https://docs.docker.com/engine/install/) on Ubuntu 24.04. Other Linux distributions may work as well, but have not been tested. Alternatives like `podman` or outdated Docker versions from other package repositories are not supported.

Install Docker:

* [Docker Engine for Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
* For Cuda GPU support also install the [NVIDIA Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/index.html)

Install packages:

```bash
sudo apt update
sudo apt install git python3-venv
```

Optional: add the Ubuntu user to the `docker group` to access the docker-engine without `sudo` and restart the operating system afterwards.

```bash
sudo usermod -a -G docker <username>
```

Check if Docker is working:

```bash
docker run --rm hello-world
```

### Windows

The EMPAIA Platform Deployment supports the usage of WSL 2 (Windows Subsystem for Linux) with Docker for Windows.

Install and configure the following components:

* [Windows Terminal](https://www.microsoft.com/de-de/p/windows-terminal/9n0dx20hk701)
* [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/)
* [Ubuntu 24.04 on WSL](https://apps.microsoft.com/detail/9nz3klhxdjp5) with [upgrade to WSL2](https://docs.microsoft.com/en-us/windows/wsl/install#upgrade-version-from-wsl-1-to-wsl-2)
* [Docker Desktop for Windows](https://docs.docker.com/desktop/windows/install/) with [WSL 2 backend](https://docs.docker.com/desktop/windows/wsl/)
* Integration of Docker Desktop for Windows with Ubuntu 24.04 on WSL 2 [enabled](https://docs.docker.com/desktop/windows/wsl/#install)

Open WSL 2 shell in Windows Terminal and install package to create a virtual environment in Python 3:

```bash
sudo apt update
sudo apt install git python3-venv
```

Optional: add the Ubuntu user to the `docker group` to access the docker-engine without `sudo` and open a new shell afterwards:

```bash
sudo usermod -a -G docker <username>
```

Check if Docker is accessible from WSL 2:

```bash
docker run --rm hello-world
```

## Clone the Platform Deployment Repository

Start the setup by cloning the Platform Deployment repository:

```bash
git clone --recurse-submodules \
    https://gitlab.com/empaia/integration/platform-deployment.git
cd platform-deployment
```

## Prepare the Platform Environment

Complete the setup of the Platform by creating a Python virtual environment and installing all necessary Python dependencies.

### Create a Python Virtual Environment

A [Python virtual environment](https://docs.python.org/3/library/venv.html) is used to install Python extensions without affecting the global Python installation. This prevents conflicts when different Python applications require different versions of the same Python extensions. Create and activate the virtual environment by running the following commands:

```bash
python3 -m venv .venv
source .venv/bin/activate
```

?> **Note:** This virtual environment must be active for most command line instructions in this documentation. If a command line fails, then check if the virtual environment is active.

### Install Python Dependencies

The Python dependencies that are defined in the [pyproject.toml](https://gitlab.com/empaia/integration/platform-deployment/-/blob/main/pyproject.toml) are managed by [Poetry](https://python-poetry.org/), which is a package manager for Python applications. Run the following command to install the dependencies:

```bash
poetry install
```
