# Import Custom Data

Custom data can be imported at any time. For the initial setup, it is suggested to skip this part first and proceed with the [verification of the Deployment](docs/deployment_verification.md#).

Cases and WSIs can be added over the given end user web interface of the [Data Management Client](https://gitlab.com/empaia/integration/data-management-client). It is furthermore possible to automate this step like done in the Platform Tests.

## Prepare WSIs

Move the desired WSIs into the directory that was specified under [COMPOSE_HOST_PATH_TO_WSIS](docs/deployment_configuration.md#modify-the-env-file).

## Create Definition Files

Create new Definition Files for Cases, WSIs, Questionnaires and Apps e.g.:

* `platform_tests/v3/resources/custom_cases.json`
* `platform_tests/v3/resources/custom_wsis.json`
* `platform_tests/v3/resources/custom_questionnaires.json`
* `platform_tests/v3/resources/custom_apps.json`

The following sections will describe the specification needed for each Definition File.

### Case Definition File

The Case Definition File specifies a Case and references the WSIs contained in it. It has the following structure:
<!-- tabs:start -->
<!-- tab:File Structure -->
```json
{
  "<case group>": {
    "<case key>": {
      "name": "<Case title>",
      "description": "<Case description>",
      "slides": [0, ..., n],
      "n_slides": 1,
      "indication": "<Case indication>",
      "procedure": "<Case procedure>"
    }
  }
}
```
<!-- tab:Example -->
```json
{
  "CUSTOM_CASES": {
    "TEST_CASE": {
      "name": "Test Case",
      "description": "Custom Test Case",
      "slides": [0, 1, 2, 2, 2],  // The WSI at index 2 is contained three times, which could be due to a Test Case that requires a Case with five WSIs, but only three different WSIs are available.
      "indication": "ICCR_PROSTATE_CANCERS",
      "procedure": "CORE_NEEDLE_BIOPSY"
    }
  }
}
```
<!-- tabs:end -->

The `slides` attribute contains a list of indices of the WSIs that would be specified in the [WSI Meta Data Definition File](#wsi-meta-data-definition-file). The indices of the WSIs can occur multiple times within the list. For example, if a Test Case requires ten WSIs, but only seven exist, the remaining three can refer to already referenced WSIs.

The `n_slides` attribute can be used instead of the `slides` attribute to implicitly define the WSI list. It will always start with the WSI at index 0 and add the given number of WSIs into the list. If the given number is greater than the length of the WSI list, then the WSIs indices beginning from index 0 are added to the WSI list. This repeats until the WSI list length is equal to the given number.

The `indication` attribute specifies the indication for a Case, whereas the `procedure` attribute can be used to specify the procedure that was used in order to obtain the specimen within the WSIs for a Case. Both attributes require specific values and are optional. Refer to this [CSV file](https://gitlab.com/empaia/integration/definitions/-/blob/main/tags/tags.csv) for a list of possible values. Look at the rows with the corresponding `TAG_GROUP`: `INDICATION` for `indication` and `PROCEDURE` for `procedure`. The values are specified in the `TAG_NAME_ID` column.

### WSI Meta Data Definition File

The WSI Meta Data Definition File specifies the meta data that will be generated for an existing WSI. It has the following structure:

<!-- tabs:start -->
<!-- tab:File Structure -->
```json
[
  {
    "path": "<relative path from COMPOSE_HOST_PATH_TO_WSIS to WSI file>",
    "stain": "<stain>",
    "tissue": "<tissue>",
    "block": "<block>"
  }
]
```
<!-- tab:Example -->
```json
[
    {
        "path": "Aperio/CMU-1.svs",
        "stain": "H_AND_E",
        "tissue": "SKIN",
        "block": "CUSTOM_BLOCK_1"
    },
    {
        "path": "Aperio/CMU-1.svs",
        "stain": "H_AND_E",
        "tissue": "SKIN",
        "block": "CUSTOM_BLOCK_2"
    }
]
```
<!-- tabs:end -->

The `path` attribute is relative to the file path [COMPOSE_HOST_PATH_TO_WSIS](docs/deployment_configuration.md#modify-the-env-file) directory to the WSI file.

The `stain` and `tissue` attributes require specific values. Refer to this [CSV file](https://gitlab.com/empaia/integration/definitions/-/blob/main/tags/tags.csv) for a list of possible values. Look at the rows with the corresponding `TAG_GROUP`: `STAIN` for `stain` and `TISSUE` for `tissue`. The values are specified in the `TAG_NAME_ID` column.

### Questionnaire Definition File

The Questionnaire Definition File references one or multiple [Fast Healthcare Interoperability Resources (FHIR) Questionnaire](https://www.hl7.org/fhir/questionnaire.html) Resources, whereas each Questionnaire Resource represents a structured report. It has the following structure:

<!-- tabs:start -->
<!-- tab:File Structure -->
```json
[
    {
      "path": "<relative path to Questionnaire Resource>",
      "selector": "<namespace that specifies Questionnaire Resource>",
      "indication": "<Case indication>",
      "procedure": "<Case indication>"
    }
]
```
<!-- tab:Example -->
```json
[
    {
      "path": "platform_tests/v3/resources/sample_questionnaires/sample_questionnaire.json",
      "selector": "org.empaia.my_vendor.my_app.v3.1.selectors.something.something-else",
      "indication": "ICCR_PROSTATE_CANCERS",
      "procedure": "CORE_NEEDLE_BIOPSY"
    }
]
```
<!-- tabs:end -->

The `path` attribute expects a relative path to a valid Questionnaire Resource. Ensure that all required properties of a FHIR Questionnaire are set. For more information about FHIR Resources refer to the [FHIR Documentation](https://www.hl7.org/fhir/documentation.html).

The `selector` attribute is a unique value that determines a Questionnaire Resource. In order for a preprocessing App to correctly locate and use a Questionnaire Resource, ensure that the `selector` attribute in its EAD matches the `selector` attribute in the Questionnaire Definition File.

The `indication` and `procedure` are used in combination with the `selector` attribute to create a unique mapping. In order for an App to correctly match a Questionnaire to a slide, the `indication` and `procedure` attributes in the Questionnaire Definition File need to be identical to the attributes of a slides Case. Refer to the [Case Definition File](#case-definition-file) for more information on these attributes.

?> The `test_questionnaires.json` should not be modified, because it is used as the Questionnaire Definition File for the Platform Tests.

### App Definition File

The App Definition File specifies which Apps should be written to the Marketplace Service Mock and AAA Service mock. It has the following structure:

<!-- tabs:start -->
<!-- tab:File Structure -->
```json
{
    "APPS": [
        {
            "<version>":{
                "ead": "<relative path to ead.json>",
                "configuration": "<relative path to config.json>",
                "status": "<status>",
                "registry": "<container registry entry of Docker image>"
            }
        }
    ]
}
```
<!-- tab:Example -->
```json
{
  "APPS": [
      {
        "ead": "../../sample_apps/sample_apps/invalid/jes_test/_01_app_behaviour/ead.json",
        "configuration": null,
        "status": "ACCEPTED",
        "registry": "registry.gitlab.com/empaia/integration/sample-apps/org-empaia-dailab-jestestapp:v1"
      },
      {
        "ead": "../../sample_apps/sample_apps/valid/tutorial/_01_simple_app/ead.json",
        "configuration": null,
        "status": "ACCEPTED",
        "registry": "registry.gitlab.com/empaia/integration/sample-apps/org-empaia-vendor_name-tutorial_app_01:v1"
      }
  ]
}
```
<!-- tabs:end -->

The `ead` attribute expects a relative path to an EMPAIA App Description File (EAD). For more information about the EAD refer to the [App Developer Documentation](https://developer.empaia.org/app_developer_docs/v3/#/).

The `configuration` attribute is optional and may be `null`. If set, it must be a relative path to a JSON file, which defines additional configuration data for the App. For more information refer to the [EMPAIA App Test Suite (EATS)](https://developer.empaia.org/app_developer_docs/v3/#/eats/eats?id).

The `status` attribute is required to have the value `ACCEPTED`, otherwise it won't be possible to select the App from the Marketplace.

The `registry` attribute defines a Docker registry URL for the Docker image that provides the actual App.

?> The [sample_apps.json](https://gitlab.com/empaia/integration/platform-tests/-/blob/main/global_services/resources/sample_apps.json) file should not be modified, because it is used as the App Definition File for the Platform Tests.

## Run the Data Generator

Run the data generators with the following commands:

```bash
python3 -m \
  platform_tests.v3.data_generators.medical_data_service.generate_data \
  --cases-file platform_tests/v3/resources/custom_cases.json \
  --wsis-file platform_tests/v3/resources/custom_wsis.json \
  --questionnaires-file platform_tests/v3/resources/custom_questionnaires.json

python3 -m \
  platform_tests.global_services.data_generators.generate_apps \
  --apps-file platform_tests/v3/resources/custom_apps.json
```
