# Verify the Deployment

To verify whether the Deployment was successfully set up, run the Platform Tests and inspect the Workbench Client's
web frontend.

## Run the Tests

The [Platform Tests](https://gitlab.com/empaia/integration/platform-tests) are implemented as Python scripts in the `platform_tests` directory. Run all Platform Tests with the following command:

```bash
pytest platform_tests/v3/tests/ --maxfail=1
```

## Inspect Web Clients

* The Data Management Client is accessible in a web browser at [http://localhost:10054](http://localhost:10054)
* The Workbench Client 3.0 is accessible in a web browser at [http://localhost:10057](http://localhost:10057)


Both clients are web frontends of the EMPAIA Platform. The Data Management Client can be used for the creation of new Cases and the upload of WSIs to them. The Workbench Client allows browsing and opening these Cases. Here, all WSIs of a Case are previewed. It furthermore allows the addition of Apps from the EMPAIA Marketplace and the creation of examinations that are run on the WSIs.

If all of these functionalities are available, the Deployment was successfully set up.
