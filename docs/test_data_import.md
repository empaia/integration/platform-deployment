# Import Test Data

In order to successfully verify the Platform setup and execute the Platform Tests, the required Apps, Cases, Questionnaires and WSIs must be imported into the Platform.

## Download Sample WSI

The test Cases require the Aperio test WSI [CMU-1.svs](http://openslide.cs.cmu.edu/download/openslide-testdata/Aperio/) from OpenSlide.
Save it in a subdirectory called `Aperio` within the directory that the `COMPOSE_HOST_PATH_TO_WSIS` variable points to in the `.env`.

To run the Platform Tests locally, the variable `PYTEST_SCRIPT_PATH_TO_WSIS` in `.env` must point to the same directory as `COMPOSE_HOST_PATH_TO_WSIS`.

?> For a local deployment, `PYTEST_SCRIPT_PATH_TO_WSIS` and `COMPOSE_HOST_PATH_TO_WSIS` should be identical, but in GitLab CI pipelines the values are different and are defined in the `variables` section of the `gitlab-ci.yml` file.

## Generate the Test Data

Run data generators with the following commands:

```bash
python3 -m \
  platform_tests.v3.data_generators.medical_data_service.generate_data \
  --cases-file platform_tests/v3/resources/test_cases.json \
  --wsis-file platform_tests/v3/resources/test_wsis.json \
  --questionnaires-file platform_tests/v3/resources/test_questionnaires.json

python3 -m \
  platform_tests.global_services.data_generators.generate_apps \
  --apps-file platform_tests/global_services/resources/sample_apps.json
```

?> Do not modify the files `test_cases.json`, `test_wsis.json`, `test_questionnaires.json` and `sample_apps.json`. They contain the data for the Platform Tests. If any changes are made, the Tests might fail.