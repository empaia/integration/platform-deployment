# Service Development

Unreleased development versions of a Service can be tested locally with the Platform Tests, by simply overwriting the original Service image with the new one.

For this, first go into the repository of the Service under development and build the Docker image:

```bash
docker build . -t my-service:dev  # image name can be chosen freely
```

Then edit the `docker-compose.yml` and replace the Service's image with (in this case) `my-service:dev`. Restart the Service with the following command:

```bash
docker compose up -d  # note that only changed Services are being restarted
```

Then run your Platform Tests.

```bash
pytest platform_tests/v3/tests/ --maxfail=1
```

Repeat these steps to redeploy code changes of a Service.
