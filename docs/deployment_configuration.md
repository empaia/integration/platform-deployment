# Deployment Configuration

The EMPAIA Platform Deployment is intended for development and testing.

## Configure the Deployment

The following list shows the most important files:


```bash
platform-deployment
│
│   # The .env file defines the environment variables for all Docker
│   # containers (see docker-compose.yml). It does not exist initially.
│   # Create it by copying and editing the sample.env template
│   # (see below).
├── .env
│
│   # Template for creating the .env file
├── sample.env
│
│   # Configuration for running the Docker containers
├── docker-compose.yml
│
│   # Configuration for the Job Execution Service (JES) when developing
│   # behind a proxy server
└── jes_docker_config
    └── config.json
```

### Create the .env file

The `.env` file defines the environment variables for all Docker containers defined in [docker-compose.yml](https://gitlab.com/empaia/integration/platform-deployment/-/blob/main/docker-compose.yml). Copy the [sample.env](https://gitlab.com/empaia/integration/platform-deployment/-/blob/main/sample.env) as `.env` and place it into the same directory:

```bash
cp sample.env .env
```

### Modify the .env file

Look for the `COMPOSE_HOST_PATH_TO_WSIS` and `PYTEST_SCRIPT_PATH_TO_WSIS` variables and specify the path to the directory of where the WSIs are stored. These directories will be mounted into the Docker containers allowing them to access the WSIs. View the following examples:

```.env
COMPOSE_HOST_PATH_TO_WSIS=/home/user/my_wsis
PYTEST_SCRIPT_PATH_TO_WSIS=/home/user/my_wsis
```

?> For a local deployment both variables usually point to the same path. In some edge cases (e.g. automated test pipeline) the values might differ.
