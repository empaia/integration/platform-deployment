# Start the Platform Services

The Platform Services will run in Docker containers as configured in the `docker-compose.yml` file.

?> The `docker compose` commands in the following sections expect that the deployment directory, which contains the `docker-compose.yml` file, is the current working directory. Also note that on POSIX platforms, the `docker` and `docker compose` commands need to be run with certain privileges, for example by running them with [sudo](https://www.unix.com/man-page/posix/8/sudo/).

First, the Docker images need to be pulled. Then the Docker containers can be started.

## Pull the Docker Images

Pull the Docker images through `docker compose` by running:

```bash
docker compose pull
```

It will download the images that are referenced in `docker-compose.yml` from the container registries.

## Start the Docker Containers

Starting the Docker containers will take about 10 to 20 seconds. Until then, Services might not yet be available. This is important to consider when trying to import data into the Platform.

```bash
docker compose up -d
```
