# Advanced

## Stop and Start the Platform Services

The following command will stop and delete all containers, but keep all Docker volumes as well as the Platform data. The data will be available, if the Services are started up again.

```bash
# Stop and delete Services
docker compose down

# Start Services in background again
docker compose up -d
```

## Data Reset

The following commands will erase all existing data.

```bash
# Remove all Docker containers and volumes and restart the Docker containers
docker compose down -v
docker compose up -d

# Wait until the Docker containers with the Platform Services have finished
# starting:
python3 scripts/wait_for_services.py docker-compose.yml -v --all-services --ignore data-management-client workbench-client

```

## Update a Deployment

```bash
# Update Platform Deployment repository
git pull
git submodule update --init --recursive

# Update .env file:
cp sample.env .env  # edit as described in configuration docs

# Pull updated Docker images
docker compose pull
```

The Platform Deployment is now updated, however the running Platform Services are still old.
Follow section [Data Reset](#data-reset) to finish the update.

?> If it is desired to keep the Platform data and no breaking
changes were introduced to the database, then a data reset is not needed. Simply restart the Docker containers with `docker compose up -d`.

## Job Execution Service configuration

Depending on the given App or the available hardware, it may be necessary or desired to adjust the configuration of the Job Execution Service.

### Enable GPU support

To enable GPU support for all Apps modify the environment settings in the `.env` file as followed:

```.env
JES_WORKER_STANDALONE_GPU=;
```

?> GPU workers are defined by hosts and devices variables. If `HOST` is omitted, the local Docker host is used. Omitting `DEVICES` will mount all available GPU devices into the App container.

In order to specify which GPU devices should be used by Apps, configure the Services accordingly. It is also possible to assign additional hosts to different workers. Detailed information about available configurations such as [Job Priority & Scheduling](https://gitlab.com/empaia/services/job-execution-service/-/blob/master/README.md#job-priority--scheduling) and [Environment Variables](https://gitlab.com/empaia/services/job-execution-service/-/blob/master/README.md#environment-variables) can be found on [GitLab](https://gitlab.com/empaia/services/job-execution-service/-/blob/master/README.md).

?> **Remark:** The Platform Deployment only supports the settings `JES_WORKER_STANDALONE_GPU` and `JES_WORKER_STANDALONE_CPU`. All other worker settings will be ignored (but they must be syntactically correct if set).

## Access OpenAPI Docs

As soon as the Deployment is running, the OpenAPI documentation of all Services can be accessed.

OpenAPI docs for official EMPAIA API specifications:

- Workbench Service (v2 API): <http://localhost:10001/v2/docs>
- Workbench Service (v2 Scopes API): <http://localhost:10001/v2/scopes/docs>
- Workbench Service (v2 Frontends API): <http://localhost:10001/v2/frontends/docs>
- Workbench Service (v3 API): <http://localhost:10001/v3/docs>
- Workbench Service (v3 Scopes API): <http://localhost:10001/v3/scopes/docs>
- Workbench Service (v3 Frontends API): <http://localhost:10001/v3/frontends/docs>
- App Service (v0 API): <http://localhost:10009/v0/docs>
- App Service (v3 API): <http://localhost:10009/v3/docs>
- Medical Data Service (v1 API): <http://localhost:10006/v1/docs>
- Medical Data Service (v3 API): <http://localhost:10006/v3/docs>
- ID Mapper Service: <http://localhost:10007/docs>
- Job Execution Service: <http://localhost:10008/docs>

OpenAPI docs for internal usage:

- Workbench Service (Test API): <http://localhost:10001/docs>
- App Service (Test API): <http://localhost:10009/docs>
- Medical Data Service (Test API): <http://localhost:10006/docs>
- Medical Data Service (Private v1 API): <http://localhost:10006/private/v1/docs>
- Medical Data Service (Private v3 API): <http://localhost:10006/private/v3/docs>
- Upload Service: <http://localhost:10005/docs>

!> If any exposed default ports of a Service were changed, adapt the port in the above URL accordingly!

## Debugging

When encountering any problems during the process of configuration, follow the recommended steps listed below for debugging:

1. Run the `docker compose ps` command to check if all Docker containers from the `docker-compose.yml` are running
2. Run the Platform Tests
