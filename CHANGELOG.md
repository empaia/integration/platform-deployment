# 0.12.2

* fix links in docs

# 0.12.1

* missing backslash in readme

# 0.12.0

* integrated FHIR based structured reports to support EMP-0105

# 0.11.4

* updated dependencies

# 0.11.3

* updated dependencies

# 0.11.2

* updated dependencies

# 0.11.0

* Case Data Partitioning

# 0.10.0

* added indication and procedure tags to case model to support EMP-0105

# 0.9.2

* changed rsa dirs to avoid mounting over the /opt/app/bin dirs

# 0.9.1

* updated dependencies

# 0.9.0

* restructured and simplified directories
* restructured and simplified docs
* updated services
* include pixelmap updates and workbench-service tests for EMP-0107 and EMP-0108

# 0.8.0

* updated annotation-service and added pixelmapd

# 0.7.13

* removed auth deployment
* cleaned up proxy settings

# 0.7.12

* updated tests and WBS

# 0.7.11

* updated for multi / single user

# 0.7.10

* fix renovate
  * `LS_ROOT_PATH=/` seems to be not valid anymore, must be `LS_ROOT_PATH=""`

# 0.7.9

* updated documentation

# 0.7.8

* renovate
* renamed `PATH_TO_WSIS` env variables

# 0.7.6

* updated docs

# 0.7.5

* updated dependencies

# 0.7.4

* renovate
* new clinical data service integration

# 0.7.3

* updated dependencies

# 0.7.1

* updated dependencies

# 0.7.0

* added pixelmaps

# 0.6.109

* updated dependencies

# 0.6.108

* updated dependencies

# 0.6.107

* updated dependencies

# 0.6.106

* updated dependencies

# 0.6.105

* updated dependencies

# 0.6.104

* updated dependencies

# 0.6.103

* updated dependencies

# 0.6.102

* updated dependencies

# 0.6.101

* updated dependencies

# 0.6.100

* update ruamel.yaml to ^0.18.5

# 0.6.99

* updated dependencies

# 0.6.98

* updated dependencies

# 0.6.97

* added JES configuration section to docs

# 0.6.96

* updated dependencies

# 0.6.95

* updated dependencies

# 0.6.94

* updated dependencies

# 0.6.93

* updated dependencies

# 0.6.92

* updated dependencies

# 0.6.91

* updated dependencies

# 0.6.90

* updated dependencies
* added ENV to upload-service (auth):
  * LS_REWRITE_URL_IN_WELLKNOWN: ${IDP_URL}
  * this is a **quickfix**. else the service will concat IDP url and urls in wellknown, e.g. to
    * <http://keycloak:8080/auth/realms/EMPAIA/http://keycloak:8080/auth/realms/EMPAIA/protocol/openid-connect/certs>
  * so there is a bug with the default value of LS_REWRITE_URL_IN_WELLKNOWN
    * TODO: find and fix bug

# 0.6.88

* Updated JES config

# 0.6.87

* updated dependencies

# 0.6.86

* updated dependencies

# 0.6.85

* updated dependencies

# 0.6.84

* updated dependencies

# 0.6.83

* updated dependencies

# 0.6.82

* updated dependencies

# 0.6.81

* updated dependencies

# 0.6.80

* updated dependencies

# 0.6.79

* updated dependencies

# 0.6.78

* updated dependencies

# 0.6.76

* updated dependencies

# 0.6.74

* updated dependencies

# 0.6.73

* updated dependencies

# 0.6.72

* updated dependencies

# 0.6.71

* updated dependencies

# 0.6.70

* updated dependencies

# 0.6.69

* updated dependencies

# 0.6.68

* updated dependencies

# 0.6.67

* updated dependencies

# 0.6.66

* updated dependencies

# 0.6.65

* updated dependencies

# 0.6.64

* updated dependencies

# 0.6.63

* updated dependencies

# 0.6.62

* fixed errors in docs

# 0.6.61

* updated dependencies

# 0.6.60

* updated dependencies

# 0.6.59

* set new env for MDS / App Service:
  * MODELS_DISABLE_POST_VALIDATION: "True"

# 0.6.57

* updated dependencies

# 0.6.56

* updated dependencies

# 0.6.55

* updated dependencies

# 0.6.54

* updated dependencies

# 0.6.53

* updated dependencies

# 0.6.52

* updated dependencies

# 0.6.51

* updated dependencies

# 0.6.50

* updated dependencies

# 0.6.49

* updated dependencies

# 0.6.47

* updated services and custom keycloak

# 0.6.46

* updated dependencies

# 0.6.45

* updated dependencies

# 0.6.44

* updated dependencies

# 0.6.43

* updated dependencies

# 0.6.42

* updated keycloak realm export in auth config of system dev docs

# 0.6.41

* fix for wbc3 port in auth compose file

# 0.6.40

* updated dependencies

# 0.6.39

* updated dependencies

# 0.6.38

* updated dependencies

# 0.6.37

* updated dependencies

# 0.6.36

* updated dependencies

# 0.6.35

* updated dependencies

# 0.6.34

* updated dependencies

# 0.6.33

* updated dependencies

# 0.6.32

* updated dependencies

# 0.6.31

* updated dependencies

# 0.6.30

* updated dependencies

# 0.6.29

* updated dependencies

# 0.6.28

* updated dependencies

# 0.6.27

* updated dependencies

# 0.6.26

* updated dependencies

# 0.6.24

* removed v1 tests and ci steps

# 0.6.23

* updated dependencies

# 0.6.22

* updated dependencies

# 0.6.21

* updated dependencies

# 0.6.20

* updated dependencies

# 0.6.19

* updated dependencies

# 0.6.18

* updated dependencies

# 0.6.17

* updated dependencies

# 0.6.16

* updated dependencies

# 0.6.15

* updated dependencies

# 0.6.13

* changed logo

# 0.6.12

* updated dependencies

# 0.6.11

* updated dependencies

# 0.6.10

* updated dependencies

# 0.6.9

* updated dependencies

# 0.6.8

* updated dependencies

# 0.6.7

* updated dependencies

# 0.6.6

* updated dependencies

# 0.6.5

* updated dependencies

# 0.6.4

* updated dependencies

# 0.6.3

* updated dependencies

# 0.6.2

* updated dependencies

# 0.6.1

* updated dependencies

# 0.6.0

* upgraded keycloak from custom 16.x docker image to custom 21.0.1

# 0.5.86

* updated dependencies

# 0.5.85

* updated dependencies

# 0.5.84

* fixed error in documentation

# 0.5.83

* updated dependencies

# 0.5.82

* updated dependencies

# 0.5.81

* updated dependencies

# 0.5.80

* updated dependencies

# 0.5.79

* updated dependencies

# 0.5.78

* updated dependencies

# 0.5.77

* updated dependencies

# 0.5.76

* updated dependencies

# 0.5.75

* updated dependencies

# 0.5.74

* updated dependencies

# 0.5.73

* updated dependencies

# 0.5.72

* updated dependencies

# 0.5.71

* updated dependencies

# 0.5.70

* updated dependencies

# 0.5.69

* updated dependencies

# 0.5.68

* updated dependencies

# 0.5.67

* updated dependencies

# 0.5.66

* updated dependencies

# 0.5.65

* updated dependencies

# 0.5.64

* updated dependencies

# 0.5.63

* updated dependencies

# 0.5.62

* updated dependencies

# 0.5.61

* updated dependencies

# 0.5.60

* updated dependencies

# 0.5.59

* updated dependencies

# 0.5.58

* updated dependencies

# 0.5.57

* updated dependencies

# 0.5.56

* updated dependencies

# 0.5.55

* updated dependencies

# 0.5.54

* updated dependencies

# 0.5.53

* updated dependencies

# 0.5.52

* updated dependencies

# 0.5.51

* updated dependencies

# 0.5.50

* updated dependencies

# 0.5.49

* update platform_tests
* updated dependencies

# 0.5.48

* updated dependencies

# 0.5.47

* updated dependencies

# 0.5.46

* updated dependencies

# 0.5.45

* updated dependencies

# 0.5.44

* added WBC3
* Edited documentation accordingly

# 0.5.43

* updated dependencies

# 0.5.42

* updated platform-tests

# 0.5.41

* updated dependencies

# 0.5.40

* updated dependencies

# 0.5.39

* updated dependencies

# 0.5.38

* updated dependencies

# 0.5.36 / 37

* update submodule platform_tests

# 0.5.35

* updated dependencies

# 0.5.34

* updated dependencies

# 0.5.33

* updated dependencies

# 0.5.32

* updated dependencies

# 0.5.31

* updated dependencies

# 0.5.30

* updated dependencies

# 0.5.29

* updated dependencies

# 0.5.28

* updated dependencies

# 0.5.27

* updated dependencies

# 0.5.26

* updated dependencies

# 0.5.25

* updated dependencies

# 0.5.24

* updated dependencies

# 0.5.23

* updated dependencies

# 0.5.22

* updated dependencies

# 0.5.21

* updated dependencies

# 0.5.20

* increased postgres db containers shared memorey (shm_size: 1g)

# 0.5.19

* updated dependencies

# 0.5.18

* updated dependencies

# 0.5.17

* updated dependencies

# 0.5.16

* updated dependencies

# 0.5.15

* updated dependencies

# 0.5.14

* updated dependencies

# 0.5.13

* updated sample-apps

# 0.5.12

* updated dependencies

# 0.5.11

* updated dependencies

# 0.5.10

* updated dependencies

# 0.5.9

* updated dependencies

# 0.5.8

* updated dependencies

# 0.5.7

* added reports feature

# 0.5.6

* updated dependencies

# 0.5.5

* typo docs

# 0.5.4

* updated docs
* more tests to fix some WBS v3 bugs

# 0.5.3

* updated docs

# 0.5.2

* updated dependencies

# 0.5.1

* updated dependencies

# 0.5.0

* subomdules and servce versions include mds v3 API

# 0.4.174

* updated dependencies

# 0.4.173

* updated dependencies

# 0.4.172

* updated dependencies

# 0.4.171

* updated platform tests

# 0.4.170

* updated dependencies

# 0.4.169

* updated dependencies

# 0.4.168

* updated dependencies

# 0.4.167

* fix autoformatter black
* update platform-tests
* update sample-apps
* updated dependencies

# 0.4.165

* updated dependencies

# 0.4.164

* updated dependencies

# 0.4.163

* updated dependencies

# 0.4.162

* updated dependencies

# 0.4.161

* updated dependencies

# 0.4.160

* updated dependencies

# 0.4.159

* updated dependencies

# 0.4.158

* updated dependencies

# 0.4.157

* updated dependencies

# 0.4.156

* updated dependencies

# 0.4.155

* updated dependencies

# 0.4.154

* updated dependencies

# 0.4.153

* updated dependencies

# 0.4.152

* updated dependencies

# 0.4.151

* updated dependencies

# 0.4.150

* updated dependencies

# 0.4.149

* updated dependencies

# 0.4.148

* updated dependencies

# 0.4.147

* updated dependencies

# 0.4.146

* updated dependencies

# 0.4.145

* updated dependencies

# 0.4.144

* updated dependencies

# 0.4.143

* updated dependencies

# 0.4.142

* added new CSP settings to WBS

# 0.4.141

* updated dependencies

# 0.4.140

* updated dependencies

# 0.4.139

* updated dependencies

# 0.4.138

* updated dependencies

# 0.4.137

* updated dependencies

# 0.4.136

* updated dependencies

# 0.4.135

* updated dependencies

# 0.4.134

* updated dependencies

# 0.4.133

* updated dependencies

# 0.4.132

* updated dependencies

# 0.4.131

* updated dependencies

# 0.4.130

* updated dependencies

# 0.4.129

* updated dependencies

# 0.4.128

* updated services and tests

# 0.4.127

* updated dependencies

# 0.4.126

* updated dependencies

# 0.4.125

* updated dependencies

# 0.4.124

* updated dependencies

# 0.4.123

* updated dependencies

# 0.4.122

* updated dependencies

# 0.4.121

* updated dependencies

# 0.4.120

* updated dependencies

# 0.4.119

* removed sample_mps_v1.env and ci-pipeline steps
* added `LOGIN_WBS_ORGANIZATION_ID` to sample.env

# 0.4.118

* updated dependencies

# 0.4.117

* updated dependencies

# 0.4.116

* updated dependencies

# 0.4.115

* updated dependencies
* updated platform tests

# 0.4.114

* updated dependencies

# 0.4.113

* updated dependencies

# 0.4.112

* updated dependencies

# 0.4.111

* updated dependencies

# 0.4.110

* updated docs

# 0.4.109

* removed depedencies
* using 127.0.0.1 instead of localhost in sample.env due to slow name resolution on Windows

# 0.4.108

* updated dependencies

# 0.4.107

* updated dependencies

# 0.4.106

* added note about python version in docs

# 0.4.105

* updated mps mock
* updated tests repo

# 0.4.104

* updated dependencies

# 0.4.103

* updated dependencies

# 0.4.102

* updated dependencies
* added docker rsa-volumes for WBS, ES, JS
* updated tests and genarators

# 0.4.101

* updated dependencies

# 0.4.100

* updated docs

# 0.4.99

* updated docs

# 0.4.98

* updated dependencies

# 0.4.97

* updated dependencies

# 0.4.96

* updated dependencies

# 0.4.95

* updated dependencies

# 0.4.94

* updated dependencies

# 0.4.93

* updated dependencies

# 0.4.92

* updated dependencies

# 0.4.91

* Renamed "Service Developer Documentation" to "System Developer Documentation"

# 0.4.90

* updated dependencies

# 0.4.89

* updated dependencies

# 0.4.88

* updated dependencies

# 0.4.87

* updated dependencies

# 0.4.86

* updated dependencies

# 0.4.85

* updated dependencies

# 0.4.84

* updated dependencies

# 0.4.83

* updated WBS

# 0.4.82

* updated dependencies

# 0.4.81

* updated tests
* updated WBS

# 0.4.80

* updated dependencies

# 0.4.79

* updated annotation-service

# 0.4.78

* updated dependencies

# 0.4.77

* updated dependencies

# 0.4.76

* updated dependencies

# 0.4.75

* updated dependencies

# 0.4.74

* updated dependencies

# 0.4.73

* updated dependencies

# 0.4.72

* updated dependencies

# 0.4.71

* updated dependencies

# 0.4.70

* updated dependencies

# 0.4.69

* updated dependencies

# 0.4.68

* updated WBS and platform tests

# 0.4.67

* updated dependencies

# 0.4.66

* updated dependencies

# 0.4.65

* updated dependencies

# 0.4.64

* updated dependencies

# 0.4.63

* updated dependencies

# 0.4.62

* updated dependencies

# 0.4.61

* updated dependencies

# 0.4.60

* updated dependencies

# 0.4.59

* updated dependencies

# 0.4.58

* updated platform tests and sample apps
* updated WBS

# 0.4.57

* updated dependencies

# 0.4.56

* updated dependencies

# 0.4.55

* updated dependencies

# 0.4.54

* updated dependencies

# 0.4.53

* updated dependencies

# 0.4.51

* updated dependencies

# 0.4.51

* updated dependencies
* renamed PYTEST_USE_MPS_V1_ROUTES to USE_MPS_V1_ROUTES

# 0.4.50

* updated dependencies

# 0.4.49

* updated dependencies

# 0.4.48

* updated mps mock to v1 routes

# 0.4.47

* updated dependencies

# 0.4.46

* updated dependencies

# 0.4.45

* updated dependencies

# 0.4.44

* updated dependencies

# 0.4.43

* minor fix for 0.4.41

# 0.4.42

* updated dependencies

# 0.4.41

* fix env var test_container_path_to_wsis default value

# 0.4.40

* updated dependencies

# 0.4.39

* updated dependencies

# 0.4.38

* updated dependencies

# 0.4.37

* updated dependencies

# 0.4.36

* updated dependencies

# 0.4.35

* updated dependencies

# 0.4.34

* updated dependencies

# 0.4.33

* updated ci
* updated tests

# 0.4.32

* updated dependencies

# 0.4.31

* fixed volumes

# 0.4.30

* moved versions into docker-compose again (automatic version upgrades will be added in a later MR)
* added frontend components
* using new versioned releases for frontends
* added script to check version equality across deployments
* simplified documentation accordingly

# 0.4.29

* added app-ui frame-ancestors wild-card "*" for development to overwrite secure default "'self'"

# 0.4.28

* updated to new JES version & renamed env vars

# 0.4.27

* updated tests

# 0.4.26

* updated tests and services

# 0.4.25

* ignore non valid versions in get-versions script

# 0.4.24

* updated docs

# 0.4.23

* changed XYZ_CORS_ALLOWED_ORIGINS to ["*"], because we do not use CORS anymore since the browser credentials have been
disabled

# 0.4.22

* updated services and adapted to wbs 0.5.87

# 0.4.21

* Updated services and removed `/v1` for WBC1.0

# 0.4.20

* new sample app demonstrating failure and test for it

# 0.4.19

* updated tests and services

# 0.4.18

* updated tests and services
* updated postgres version
* updated keycloak version
* set additional env variables in auth deployment for openapi authentication

# 0.4.17

* updated tests and services

# 0.4.16

* updated tests and services

# 0.4.15

* updated tests

# 0.4.14

* updated tests

# 0.4.13

* updated WBS
* updated platform tests

# 0.4.12

* updated services for small fixes

# 0.4.11

* added WBC 2.0 scope tests for slides and data routes

# 0.4.10

* added WBC 2.0 Sample App 01 to data generators and tests

# 0.4.9

* added tests for service to service authentication
* added read only tests
* added keycloak docker volume

# 0.4.8

* update platform_tests
* required `/v1` added in deyployments at wbc env var `WBC_WBS_SERVER_API_URL: http://localhost:10001/v1`
  * due to now supporting `v1` and `v2` in parallel

# 0.4.7

* fix US configuration (LS_REQUIRE_ID_META_DATA: "True")
* added vol for US info files

# 0.4.6

* added upload-service and data-management-client

# 0.4.5

* update platform_tests

# 0.4.4

* docs and comments in sample.env

# 0.4.3

* removed automatic client sub determination

# 0.4.2

* added auth deployment

# 0.4.1

* generated cases from data generators are now placed under `deployments/<deployment>/data` instead of `deployments//data`

# 0.4.0

* refactored cases and app data generators

# 0.3.1

* service versions are now defined by env variables
  * can be fetched with scripts/get_services_versions.py
    * also automatically done by ci script
    * tested, working versions are stored inside deployment/.../valid_versions/YY-MM-DD-x.env

# 0.2.0

* initial commit
