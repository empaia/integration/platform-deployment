# System Developer Documentation

Welcome to the EMPAIA System Developer Docs. The following guides provide you with the required information to setup a simple local deployment of the EMPAIA Platform Services for open-source developers or developers of commercial clinical system. The EMPAIA Platform might serve as a blueprint for commercial systems to reimplement the EMPAIA API specifications. EMPAIA open-source components can also be used directly in commercial product.

## Contact

* If: you have found an error in this documentation, a software bug, or if would like to request a Service feature please use the [Services Issue Tracker](https://gitlab.com/empaia/integration/platform-deployment/-/issues)
* Else: contact us directly via [support@empaia.org](mailto:support@empaia.org)

## References

* [EMPAIA Platform Deployment Repository](https://gitlab.com/empaia/integration/platform-deployment)

## EMPAIA Platform Architecture

The following figure shows the EMPAIA platform architecture. The reference implementation is open source and can be deployed for development purposes as described in this documentation. The architecture diagram shows Global Serivces (purple) and Platform Services (yellow), where multiple deployments of the Platform Services (e.g. for multiple clinics) can be connected to a single deployment of the Global Services. This platform deployment for development purposes is simplified and focuses on a single deployment of the Platform Services and only contains some mock services as a replacement for the Global Services. All platform services communicate through HTTP APIs, that have been and will be specified by the EMPAIA Consortium in conjunction with System and App developers. Many concpets, including the App and Workbench APIs, are specified in the [App Developer Docs](https://developer.empaia.org/app_developer_docs/v2/).

<img src="docs/images/01_platform_architecture_reference_implementation.svg"></img>

## Third-Party Platform Integration

As shown in the following figure, system developers might first attempt a shallow integration approach with their own clinical infrastructure components. In this case, components of the EMPAIA reference implementation (yellow) can be mixed with third-party systems (orange/striped). For example, some microservices of the Medical Data Service (e.g. Clinical Data Service) can be replaced by adapters to connect to Anatomic Pathology Laboratory Information Systems (APLIS), Image Management Sytems (IMS), Vendor Neutral Archives (VNA) or Picture Archiving and Communication System (PACS). The Workbench Client (Web UI) can be replaced with a third-party frontend connecting to the Workbench API. Compatible frontends [embed App UIs](https://developer.empaia.org/app_developer_docs/v2/#/tutorial_frontend/initialization) provided by App developers via the EMPAIA marketplace.

<img src="docs/images/02_platform_architecture_shallow_integration.svg"></img>

The following figure demonstrates a full third-party integration with EMPAIA APIs. In this scenario, all components of the reference implementation are being replaced by compatible third-party systems. For example, adapters are not necessary in the storage backend, because components implement the Medical Data API directly. Please note, that not all systems must be implementated by the same system developer, because cross-vendors compatibility is ensured by following the EMPAIA API specifications.

<img src="docs/images/03_platform_architecture_deep_integration.svg"></img>

## Change Log

### 2025-02-18

* Extended EMPAIA v3 API by FHIR based structured reports functionality
  * Allows AI Apps to reference `FHIR Questionnaire` Resources that represent structured reports
  * Results of AI Apps (e.g. diagnostic scores or measurements) can be stored in a `FHIR QuestionnaireResponse` Resource that references a Questionnaire
  * Added new `harpy-service` and `blaze-fhir-server`
  * Use new `Questionnaire Definition File` via data generator to add one or multiple Questionnaire Resources to the Platform along with all required meta data
* Updated Ubuntu to 24.04 and Python to 3.12
* Reworked System Developer Docs

### 2024-08-07

* Updated services to support new pixelmaps features
  * Annotation Service now requires additional `pixelmapd` container
  * Old pixelmaps are automatically migrated to new storage format, but old data is not removed
  * Use `annotctl remove-migrated-pixelmaps` included with the Annotation Service to clean up old pixelmap data
* Restructured and simplified System Developer Docs

### 2024-04-22

* Renamed `PATH_TO_WSIS` and `PYTEST_PATH_TO_WSIS` ENV variables
  * See `sample.env` files for new ENV names

### 2024-03-28

* WSI Service functionality is now integrated in Clinical Data Service and the WSI Service has been removed
* Clinical Data Service provides a new plugin interface to read WSI data based on ZeroMQ
* Every WSI Plugin for the Clinical Data Service runs as a separate container process

### 2022-11-18 - Major Update

* Required new Features break compatibility with existing Apps
  * Raw WSI downloadble by Apps
  * Preprocessing jobs / app mode
  * Postprocessing jobs / app mode
  * Standalone jobs / app mode
* Therefore we introduced a new version for WBS, MDS and App API (`v3`)
* Seperated `data_generation` and `tests` according to API versions
  * `./platform_tests/v1`
    * WBS v1
    * MDS v1
    * App API v0
    * JES v1
  * `./platform_tests/v2`
    * same as `v1` but WBS v2
  * `./platform_tests/v3`
    * WBS v3
    * MDS v3
    * App API v3
    * JES v1

* No matter the platform API version, there is only one Auth API and one Marketplace API
  * `./platform_tests/global_service`

### 2022-07-14

* Updated platform architecture figure.
* Added additional figures showing third-party platform integration approaches.
* Added [Access OpenAPI Docs](/docs/advanced.md#access-openapi-docs) section

### 2022-07-04

* Initial version of System Developer Documentation was published.
