#!/bin/sh
if git rev-parse $1 >/dev/null 2>&1; then
  echo "tag" $1 "exists";
  exit 1;
else
  echo "tag" $1 "does not exist";
  exit 0;
fi
