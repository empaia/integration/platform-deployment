#!/usr/bin/env python
import argparse
import json
import subprocess
import tempfile
import time

from ruamel.yaml import YAML

try:
  from platform_tests.data_generators.singletons import login_manager
except ImportError:
  # platform_tests repo not in PATH/not available
  login_manager = None


class ServiceError(RuntimeError):
  pass


class ServiceResult:
  headers_for_auth = None
  login_manager_timed_out = False

  def __init__(self, service_name, url):
    self.service_name = service_name
    self._is_alive = False
    self.url = url
    self.stdout = ''
    self.stderr = ''
    self.has_sub_services = service_name == 'medical-data-service'
    self.needs_auth = False

  @property
  def is_alive(self):
    is_alive = self._is_alive
    try:
      alive_response = json.loads(self.stdout)
    except ValueError:
      alive_response = None  # don't log the exception, stdout will be printed anyway
    else:
      is_alive = self._status_is_ok(alive_response)
      if not is_alive:
        self._fetch_auth_headers_if_necessary(alive_response)
    if is_alive and self.has_sub_services:
      is_alive = False
      if alive_response is not None:
        if 'services' in alive_response:
          is_alive = True
          for name, data in alive_response['services'].items():
            if not self._status_is_ok(data):
              is_alive = False
              self._fetch_auth_headers_if_necessary(data)
              break
    return is_alive

  def _status_is_ok(self, alive_response):
    return alive_response.get('status', 'unknown') == 'ok'

  def _fetch_auth_headers_if_necessary(self, alive_response):
    if login_manager is not None and not self.login_manager_timed_out:
      if alive_response.get('detail', 'unknown').lower() == 'not authenticated':
        self.needs_auth = True
        if self.headers_for_auth is None:
          timeout = 20
          start = time.time()
          while True:
            try:
              self.headers_for_auth = login_manager.wbs_client()
              break
            except Exception as e:
              if 'Connection' in str(e):
                pass
            if (time.time()-start) < timeout:
              time.sleep(0.1)
            else:
              self.login_manager_timed_out = True
              break


  def check(self):
    cmd = ['curl', '--silent', '--show-error', self.url]
    if self.headers_for_auth is not None:
      with tempfile.TemporaryDirectory() as temp_dir:
        filename = temp_dir+'/headers'
        with open(filename, 'w') as f:
          for key, value in self.headers_for_auth.items():
            print(f"{key}: {value}", file=f)
        cmd.extend(['--header', f'@{filename}'])
        p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    else:
      p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    self._is_alive = p.returncode == 0
    self.stdout = p.stdout.decode('utf-8')
    self.stderr = p.stderr.decode('utf-8')

class ServicesData:
  def __init__(self, docker_compose_yml):
    yaml = YAML(typ="safe")
    with open(docker_compose_yml) as f:
      document = yaml.load(f)
    self.services_data = document.get("services")

  @property
  def all_service_names(self):
    return [name for name, _ in self.services_data.items() if not name.endswith('-db')]

  def get_service_alive_url(self, service_name) -> str:
    service_data = self.services_data.get(service_name)
    if service_data is None:
      raise ServiceError(f'No service with name {service_name} found.')
    else:
      ports = service_data.get("ports")
      if ports is None:
        raise ServiceError(f"Service '{service_name}' does not expose any port.")
      else:
        host_name, host_port, container_port = ports[0].split(":")
        url = f"http://{host_name}:{host_port}/alive"
    return url

class ServicesQuery:
  def __init__(self, services_data, service_names, query_all_services, ignored_services, verbose, retry, retry_delay, retry_max_time):
    retry = 60 if retry is None else int(retry)
    retry_delay = 1 if retry_delay is None else int(retry_delay)
    retry_max_time = 60 if retry_max_time is None else int(retry_max_time)
    self.verbose = verbose
    self.services_data = services_data
    self.query_all_services = query_all_services
    self.retry = retry
    self.retry_delay = retry_delay
    self.retry_max_time = retry_max_time
    if query_all_services:
      self.service_names = [
        name for name in services_data.all_service_names
        if ignored_services is None or name not in ignored_services
      ]
    else:
      self.service_names = service_names
    self.services_results = []
    for name in self.service_names:
      try:
        url = self.services_data.get_service_alive_url(name)
        self.services_results.append(ServiceResult(name, url))
      except ServiceError as e:
        if not self.query_all_services:
          print(e)

  def check_services(self):
    start = time.time()
    count = 0
    while count < max(1, self.retry):
      all_alive = True
      for result in self.services_results:
        needed_auth = result.needs_auth
        if not result.is_alive:
          result.check()
        if not result.is_alive:
          # retry once with credentials without incrementing retry count:
          if not needed_auth and result.needs_auth:
            result.check()
        if not result.is_alive:
          all_alive = False
      elapsed = time.time()-start
      if all_alive or count >= self.retry or elapsed > self.retry_max_time:
        break
      time.sleep(self.retry_delay)
      count += 1

  def are_services_alive(self):
    self.check_services()
    all_alive = True
    for result in self.services_results:
      if not result.is_alive:
        all_alive = False
      if self.verbose:
        if result.is_alive:
          print(f"Service '{result.service_name}' ({result.url})"
                f"{' and all of its sub services are' if result.has_sub_services else ' is'} alive: {result.stdout}")
        else:
          print(f"Service '{result.service_name}' ({result.url})"
                f"{' or any of its sub services' if result.has_sub_services else ''} is not alive: {result.stdout}")
        if not result.is_alive:
          print(result.stderr)
    return all_alive

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('docker_compose_yml')
  parser.add_argument('service_name', nargs='*')
  parser.add_argument('-v', action="store_true", dest="verbose")
  parser.add_argument('--all-services', action="store_true", dest="all_services")
  parser.add_argument('--ignore', nargs='+', dest="ignored_services")
  parser.add_argument('--retry')
  parser.add_argument('--retry-delay')
  parser.add_argument('--retry-max-time')
  args = parser.parse_args()
  services_data = ServicesData(args.docker_compose_yml)
  services_query = ServicesQuery(
    services_data, args.service_name, args.all_services,
    args.ignored_services, args.verbose,
    args.retry, args.retry_delay, args.retry_max_time
  )
  all_alive = services_query.are_services_alive()
  if args.verbose:
    print(f"{'All' if all_alive else 'Not all'} services are up and running.")

if __name__ == '__main__':
  main()
